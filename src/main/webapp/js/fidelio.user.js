//==UserScript==
//@name         FIDELIO Injector
//@namespace    https://id1.vx4.eu/
//@version      0.91
//@description  providing eID U2F & WebAuthn to stock browsers
//@author       ck@vx4.de
//@match        https://*/*
//@grant        GM_getResourceText
//@grant        GM_xmlhttpRequest
//@connect      id1.vx4.eu
//@require      ./cbor.js?2020010401
//@require      ./fidelio.js?2020010401
//@resource     env ../?env
//@run-at       document-start
//==/UserScript==

(function() {
 'use strict';
 if(unsafeWindow) {
     Object.defineProperty(unsafeWindow, "_FIDELIO", { writable: false, configurable:false, value: _FIDELIO });
     Object.defineProperty(unsafeWindow, "u2f", { writable: false, configurable:false, value: {} });
     Object.defineProperty(unsafeWindow.u2f, "register", { writable: false, configurable:false, value: fidelio.u2freg });
     Object.defineProperty(unsafeWindow.u2f, "sign", { writable: false, configurable:false, value: fidelio.u2fsig });
 }
})();