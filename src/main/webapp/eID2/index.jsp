<%@ page language="java"
 import="java.net.*, java.security.*, java.util.*, javax.crypto.*, javax.crypto.spec.*, de.persoapp.core.util.*"
 session="false"%><%
	final String origin = request.getHeader("origin"), baseURL = "https://" + request.getServerName()
	    + (request.getServerPort() == 443 ? "" : ":" + request.getServerPort()) + request.getContextPath(),
        ua = request.getHeader("user-agent").toLowerCase();

    // generate ID with HMAC(trust-key, random value) to ensure it originated from this server
    final byte[] rawid = new byte[32];
    SecureRandom.getInstance("SHA1PRNG").nextBytes(rawid);

 	final Mac mac = Mac.getInstance("HmacSHA256");
 	mac.init(new SecretKeySpec("HMac-Secret-2020-0001".getBytes(), "HmacSHA256"));
    final String id = Base64.getUrlEncoder().encodeToString(ArrayTool.concat(rawid, mac.doFinal(rawid)));

	System.out.println("ORIGIN: " + origin);
	System.out.println("REFERER: " + request.getHeader("referer"));

	String location = URLEncoder.encode(baseURL + "/$2/?" + request.getQueryString() + "&id=" + id);
	if (ua.matches("^.*?(android).*$")) { // Android
	    //location = "intent://127.0.0.1:24727/eID-Client?tcTokenURL=" + location + "#Intent;scheme=eid;B.android.intent.extra.RETURN_RESULT=true;S.browser_fallback_url=http%3A%2F%2Fid1.vx4.eu;end";
	    location = "eid://127.0.0.1:24727/eID-Client?tcTokenURL=" + location + "&consume";
	} else if (ua.matches("^.*?(iphone|ipad|ipod).*$")) { // Apple
	    location = "eid://127.0.0.1:24727/eID-Client?tcTokenURL=" + location + "&consume";
	} else { // Desktop
	    location = "http://127.0.0.1:24727/eID-Client?tcTokenURL=" + location;
	}

	response.setStatus(200);
	response.setContentType("application/json");
	response.setHeader("Access-Control-Allow-Origin", "*"); //origin);
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("Referrer-Policy", "origin");
%>{ "client": "<%= location %>", "result": "<%= baseURL %>/$2/?wait=<%= id %>" }
