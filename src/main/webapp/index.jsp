<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" session="false" import="java.io.*, java.net.*, java.util.*"%><%!
%><%
	// removed the complex request checking due different proxy scenarios
	// if required and applicable request pre-checking could be injected here

//    final String tlsProtocol = (String)request.getAttribute("org.apache.tomcat.util.net.secure_protocol_version");
//    final String tlsCipher = (String)request.getAttribute("javax.servlet.request.cipher_suite");
//    final String authHeader = request.getHeader("authorization");
//    final String uaHeader = request.getHeader("user-agent");

    final String hostHeader = request.getHeader("host"), query = request.getQueryString();
    try {
        if(hostHeader == null || hostHeader.isEmpty() || query == null || query.length() < 3) {
            response.setHeader("WWW-Authenticate", "Basic realm=\"" + "FIDELIO\"");
            response.setStatus(401);
        } else if("env".equals(query)) {
            out.write(request.getRequestURL().toString());
        } else {
            request.getRequestDispatcher("/x").forward(request, response);
        }
    } catch(Exception e) {
    	e.printStackTrace();
    	out.println("Error initializing service / setup incomplete.");
    }
%>